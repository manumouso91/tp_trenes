#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "tren.h"
#include "conexion.h"


void cargar_datos(char * argv[],STR_TREN *tren){

strcpy(tren->id,argv[1]);
strcpy(tren->estacion_origen,argv[2]);
strcpy(tren->tiempo_viaje,argv[3]);
printf("ingrese combustible:");
scanf(" %s",tren->combustible);
printf("Ingrese estacion destino: ");
scanf(" %s",tren->estacion_destino);
strcpy(tren->estado,"en estacion");

return ;
}

void registrarse_estacion(STR_TREN * tren, int sockfd){
  int mensaje=0;
  char sep[2]=";";
  char header[2]="1";
  char * cadena=NULL;
  if((cadena=(char *) (malloc(sizeof(STR_TREN))))==NULL){
    exit(EXIT_FAILURE);
  }
  strcpy(cadena,header);
  strcat(cadena,sep);
  strcat(cadena,tren->id);
  strcat(cadena,sep);
  strcat(cadena,tren->estacion_origen);
  strcat(cadena,sep);
  strcat(cadena,tren->estacion_destino);
  strcat(cadena,sep);
  strcat(cadena,tren->combustible);
  strcat(cadena,sep);
  strcat(cadena,tren->tiempo_viaje);
  strcat(cadena,sep);
  strcat(cadena,tren->estado);

  if((mensaje= send(sockfd, cadena,122,0))==-1){
    perror("send");
  }

  free(cadena);
  cadena=NULL;
  return ;
}
