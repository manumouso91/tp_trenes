
#ifndef TREN_H
#define TREN_H


typedef struct tren{
  char id[20];
  char estacion_origen[20];
  char estacion_destino[20];
  char tiempo_viaje[20];
  char combustible[20];
  char estado[20];
}STR_TREN;


void cargar_datos(char *argv[],STR_TREN * tren);

//envia mensaje y se mete en la cola de espera
void registrarse_estacion(STR_TREN * tren, int sockfd);

//se pregunta si el anden esta vacio x mensaje y se recibe la confirmacion, se mete en cola espera
bool solicitar_anden(STR_TREN * tren, bool estado_anden);
//informa el MOTIVO, tecnicamente se cambia un estado motivo a anden si solicita
//o paso si es partir sin solicitar
//envia mensaje
void partir(STR_TREN * tren);

//imprime
void estado(STR_TREN  tren);

#endif
