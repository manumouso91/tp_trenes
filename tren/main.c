#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "tren.h"
#include "conexion.h"




int main(int argc, char * argv[]){
  if(argc != 4){
    perror("Error");
    return -1;
  }
char ip[20];
int puerto=0;

STR_TREN * tren= NULL;
if((tren=(STR_TREN *) (malloc(sizeof(STR_TREN))))==NULL){
  exit(EXIT_FAILURE);
}

printf("Ingrese la ip de la estacion : \n");
scanf(" %s", &ip);
printf("Ingrese puerto de la estacion: \n");
scanf(" %i",&puerto);



cargar_datos(argv,tren);
conexion(ip,puerto,tren);

free(tren);

tren =NULL;

return 0;
}
