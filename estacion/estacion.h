#ifndef ESTACION_H
#define ESTACION_H

static const int MAX=30;

typedef struct tren{
  char id[20];
  char estacion_origen[20];
  char estacion_destino[20];
  char tiempo_viaje[20];
  char combustible[20];  
  char estado[20];
  int espera;
}STR_LISTA;

int obtener_header(char buffer[]);

void reestructurar_mensaje(char buffer[],STR_LISTA *tren);

void inicializar_cola_espera(STR_LISTA cola_espera[10]);

void insertar_ordenado(STR_LISTA trenes[], STR_LISTA tren, int * cantidad_trenes);

//crear un archivo de texto que vaya guardando id destino origen motivo(mensaje recibe tren)
void registrar_paso();
//se envia el primer tren en la lista de espera o el que este en el anden si esta ocupado
void enviar_tren(STR_LISTA trenes[], STR_LISTA *anden);

//matar el proceso tren si llego a destino, como?
void finalizar_recorrido(STR_LISTA *tren);

#endif
