#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "estacion.h"
#include "conexion.h"

int obtener_header(char buffer[]){
const char sep[2]=";";
char *token=NULL;
token=strtok(buffer,sep);
char header[2]=" ";
strcpy(header,token);
return atoi(header);
}

void reestructurar_mensaje(char buffer[], STR_LISTA *tren){
const char sep[2]=";";
char *token=NULL;
token=strtok(buffer,sep);
token=strtok(NULL,sep);
strcpy(tren->id,token);
token=strtok(NULL,sep);
strcpy(tren->estacion_origen,token);
token=strtok(NULL,sep);
strcpy(tren->estacion_destino,token);
token=strtok(NULL,sep);
strcpy(tren->tiempo_viaje,token);
token=strtok(NULL,sep);
strcpy(tren->combustible,token);
token=strtok(NULL,sep);
strcpy(tren->estado,token);
}

void inicializar_cola_espera(STR_LISTA cola_espera[10]){
for(int i=0;i<10;i++){
memset(cola_espera[i].id,'\0',20);
memset(cola_espera[i].estacion_origen,'\0',20);
memset(cola_espera[i].estacion_destino,'\0',20);
memset(cola_espera[i].tiempo_viaje,'\0',20);
memset(cola_espera[i].combustible,'\0',20);
memset(cola_espera[i].estado,'\0',20);
cola_espera[i].espera=0;
}
}
