#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include "estacion.h"
#include "conexion.h"


void obtener_puerto(int * puerto){
  printf("Ingrese puerto de la estacion: \n");
  scanf(" %i",puerto);
}

void sigchld_handler(int s){
    while(wait(NULL) > 0);
}
void conexion(int puerto,STR_LISTA * tren){
  int sockfd=0;
  int new_fd=0;
  struct sockaddr_in my_addr;
  struct sockaddr_in their_addr;
  int sin_size;
  struct sigaction sa;
  int yes=1;
  char buf[MAXDATASIZE];
  int mensaje=0;

  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      perror("socket");
      exit(1);
  }

  if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
      perror("setsockopt");
      exit(1);
  }

  my_addr.sin_family = AF_INET;
  my_addr.sin_port = htons(puerto);
  my_addr.sin_addr.s_addr = INADDR_ANY;
  memset(&(my_addr.sin_zero), '\0', 8);

  if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))
                                                                 == -1) {
      perror("bind");
      exit(1);
  }

  if (listen(sockfd, BACKLOG) == -1) {
      perror("listen");
      exit(1);
  }

  sa.sa_handler = sigchld_handler;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;

  if (sigaction(SIGCHLD, &sa, NULL) == -1) {
      perror("sigaction");
      exit(1);
  }

  while(1) {
      sin_size = sizeof(struct sockaddr_in);
      if ((new_fd = accept(sockfd, (struct sockaddr *)&their_addr,
                                                     &sin_size)) == -1) {
          perror("accept");
          continue;
      }
      printf("server: got connection from %s\n",
                                         inet_ntoa(their_addr.sin_addr));

      if (!fork()) { // Este es el proceso hijo

          close(sockfd); // El hijo no necesita este descriptor


          mensaje= recv(new_fd, buf, MAXDATASIZE-1, 0);
          buf[mensaje] = '\0';


          printf("Received: %s \n",buf);
          int header=obtener_header(buf);

          printf("header: %i ", header);
          switch (header){
            case 1:
                reestructurar_mensaje(buf,tren);
            break;
            default: printf("fallo");
            break;
            }


          exit(0);

      }

      close(new_fd);  // El proceso padre no lo necesita

  }

}
