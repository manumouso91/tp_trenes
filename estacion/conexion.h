#ifndef CONEXION_H
#define CONEXION_H

#define MAXDATASIZE 100
#define BACKLOG 10


void obtener_puerto(int * puerto);
void sigchld_handler(int s);
void conexion(int puerto,STR_LISTA * tren);


#endif
